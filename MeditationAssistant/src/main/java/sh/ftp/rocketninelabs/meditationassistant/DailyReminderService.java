package sh.ftp.rocketninelabs.meditationassistant;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class DailyReminderService extends JobService {
    @Override
    public boolean onStartJob(JobParameters params) {
        Log.d(MeditationAssistant.LOG_TAG, "Daily reminder job starting");

        Intent actionReminder = new Intent(MeditationAssistant.ACTION_REMINDER);
        sendBroadcast(actionReminder);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d(MeditationAssistant.LOG_TAG, "Daily reminder job stopped");
        return true;
    }
}