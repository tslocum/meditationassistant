package sh.ftp.rocketninelabs.meditationassistant;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DailyReminderReceiver extends BroadcastReceiver {
    MeditationAssistant ma = null;

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            ma = (MeditationAssistant) context.getApplicationContext();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        if (!getMeditationAssistant().getPrefs().getBoolean("pref_daily_reminder", false)) {
            getMeditationAssistant().cancelDailyReminder(context);
            return; // The user has not enabled the daily reminder
        }
        Log.d("MeditationAssistant", "onReceive in DailyReminderReceiver");

        if (intent != null && intent.getAction() != null && intent.getAction().equals(MeditationAssistant.ACTION_REMINDER)) { // otherwise, it was just an update
            Log.d("MeditationAssistant", "Received daily reminder notification intent");

            SimpleDateFormat sdf = new SimpleDateFormat("d-M-yyyy", Locale.US);
            if (getMeditationAssistant().getTimeToStopMeditate() != 0) {
                Log.d("MeditationAssistant", "Skipping daily reminder notification today, session in progress...");
            } else if (getMeditationAssistant().db.numSessionsByDate(Calendar.getInstance()) > 0) {
                Log.d("MeditationAssistant", "Skipping daily reminder notification today, there has already been a session recorded...");
            } else {
                long last_reminder = getMeditationAssistant().getPrefs().getLong("last_reminder", 0);
                if (last_reminder == 0 || getMeditationAssistant().getTimestamp() - last_reminder > 120) {
                    getMeditationAssistant().getPrefs().edit().putLong("last_reminder", getMeditationAssistant().getTimestamp()).apply();

                    Log.d("MeditationAssistant", "Showing daily reminder notification");

                    String reminderText = getMeditationAssistant().getPrefs().getString("pref_daily_reminder_text", "").trim();
                    if (reminderText.equals("")) {
                        reminderText = context.getString(R.string.reminderText);
                    }

                    NotificationCompat.Builder notificationBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.ic_notification)
                                    .setContentTitle(context.getString(R.string.meditate))
                                    .setContentText(reminderText)
                                    .setTicker(reminderText)
                                    .setAutoCancel(true);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        notificationBuilder.setChannelId("reminder");
                    }

                    if (getMeditationAssistant().getPrefs().getBoolean("pref_vibrate_reminder", true)) {
                        long[] vibrationPattern = {0, 200, 500, 200, 500};
                        notificationBuilder.setVibrate(vibrationPattern);
                    } else {
                        long[] vibrationPattern = {0, 0};
                        notificationBuilder.setVibrate(vibrationPattern);
                    }

                    if (getMeditationAssistant().getPrefs().getBoolean("pref_sound_reminder", true)) {
                        notificationBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
                    }

                    Intent notificationIntent = new Intent(context, MainActivity.class);
                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                    stackBuilder.addParentStack(MainActivity.class);
                    stackBuilder.addNextIntent(notificationIntent);
                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT | MeditationAssistant.extraPendingIntentFlags());
                    notificationBuilder.setContentIntent(resultPendingIntent);

                    Notification notification = notificationBuilder.build();

                    NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(MeditationAssistant.dailyReminderNotificationID, notification);
                } else {
                    Log.d("MeditationAssistant", "Skipping daily reminder notification today, a daily notification was recently shown...");
                }
            }
        }

        getMeditationAssistant().setDailyReminder(context);
    }

    public MeditationAssistant getMeditationAssistant() {
        return ma;
    }
}