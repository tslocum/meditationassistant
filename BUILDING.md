To build Meditation Assistant from source:

# Download

```git clone https://code.rocket9labs.com/tslocum/meditationassistant.git```

# Import

Open [Android Studio](https://developer.android.com/studio/), select *Import
project* and choose the folder downloaded above.

# Build

Connect an Android device to your PC using
[ADB](https://developer.android.com/studio/command-line/adb) or start an
[emulated device](https://developer.android.com/studio/run/emulator).

Open the *Build Variants* panel and choose a variant.
Debug variants build quickly, while release variants are optimized.

Variants:

- opensource: F-Droid
- free: Google free version
- full: Google donate version

Click the green play button labeled *Run selected configuration* to build and
install the application.
